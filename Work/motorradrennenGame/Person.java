package Work.motorradrennenGame;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;
import java.time.LocalDate;

public class Person {

	private String name;
	private String geburtsdatum;
	private int alter;
	
	public Person() {
		
	}
	
	Person(String a, String b){
		this.name = a;
		this.geburtsdatum = b;
	}
	
	public int getAlter() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.mm.yyyy");
		Date date2 = formatter.parse(geburtsdatum);
		Date date1 = new Date();
		
	    long diffInMillies = date1.getTime() - date2.getTime();
	    int diff = (int)(diffInMillies /1000/60/60/24/365);
	    System.out.println(diff);
	    this.alter = diff;
	    return diff;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}
	
	
	
}
