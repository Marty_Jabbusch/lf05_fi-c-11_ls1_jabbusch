package Schulaufgaben.Methoden;
import java.util.Scanner;

public class Fahrsimulator {

	public static double beschleunige(double anfangsgeschwindigkeit, double beschleunigung) {
		double geschwindigkeit = anfangsgeschwindigkeit + beschleunigung;
		if(geschwindigkeit > 130) {
			geschwindigkeit = 130;
		}
		if(geschwindigkeit < 0) {
			geschwindigkeit = 0;
		}
		System.out.println("Die aktuelle Geschwindigkeit betr�gt: " + geschwindigkeit);
		return geschwindigkeit;
	}
	
	public static void main(String[] args) {
		boolean b = true;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Was ist die Anfangsgeschwindigkeit?");
		double anfangsgeschwindigkeit = myScanner.nextDouble();
		while(b = true) {
			System.out.println("Was ist die Beschleunigung?");
			double beschleunigung = myScanner.nextDouble();
			anfangsgeschwindigkeit = beschleunige(anfangsgeschwindigkeit, beschleunigung);
		}
	}

}
