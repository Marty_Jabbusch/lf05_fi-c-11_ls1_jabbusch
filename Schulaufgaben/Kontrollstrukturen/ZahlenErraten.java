package Schulaufgaben.Kontrollstrukturen;
import java.util.Scanner;
import java.lang.Math;

public class ZahlenErraten{

	public static void main(String[] args) {
		int zahl = (int) (100 * Math.random());
		System.out.println(zahl);
		boolean b = false;
		System.out.println("Erraten Sie die Zahl zwischen 1 und 100");
		while(b == false){
			System.out.println("Eingabe:");
			Scanner scanner = new Scanner(System.in);
			int eingabe = scanner.nextInt();
			if(eingabe == zahl){
				b = true;
				System.out.println(eingabe + " ist die richtige Zahl");
			}
			else{
				if(eingabe < zahl){
					System.out.println("Die gesuchte Zahl ist größer");
				}
				else{
					System.out.println("Die gesuchte Zahl ist kleiner");
				}
			}
		}
		


	}
}