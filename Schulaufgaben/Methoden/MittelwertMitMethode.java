package Schulaufgaben.Methoden;
import java.util.Scanner;

public class MittelwertMitMethode {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double m;
		Scanner myScanner = new Scanner(System.in);
		zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein");
		zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl");
		m = verarbeitung(zahl1, zahl2);
		System.out.println(ausgabe(m));
	}

	public static double eingabe(Scanner myScanner, String text) {
		System.out.println(text);
		double eingabe1 = myScanner.nextDouble();
		return eingabe1;
	}
	
	public static double verarbeitung(double zahl1, double zahl2) {
		double m = ((zahl1 + zahl2)/2);
		return m;
	}
	
	public static String ausgabe(double m) {
		String ausgabe = "Mittelwert: " + m;
		return ausgabe;
	}
}
