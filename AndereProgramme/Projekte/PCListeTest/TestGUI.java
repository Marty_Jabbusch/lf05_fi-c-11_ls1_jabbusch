package PCListeTest;

import PCListeTest.mainClass;
import PCListeTest.PC;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class TestGUI implements ActionListener{
	
	static HashMap<String, PC> PCliste = new HashMap<String, PC>();
	JLabel label;
	JFrame frame; 
	JButton button;
	JPanel panel;
	//JButton buttonw;

	
	public TestGUI() {
		frame = new JFrame();
		button = new JButton("Inventrarnummern");
		label = new JLabel("CNC-Raum");
		panel = new JPanel();
		//buttonw = new JButton("Pfad der .csv-Datei eingeben");
		panel.setLayout(null);

		label.setBounds(10, 10, 10, 10);

		
		panel.setBorder(BorderFactory.createEmptyBorder(100, 100, 100, 100));
		panel.setLayout(new GridLayout());
		panel.add(button);
		panel.add(label);
		
		
		frame.add(panel, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("CNC-Raum");
		frame.pack();
		frame.setVisible(true);
		
		button.addActionListener(this);
		
		DerHandler handler = new DerHandler();
		button.addActionListener(handler);
	}
	
	
	private class DerHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			
			int count = -1;
			String path = "P:\EX-T-6\85_BA\82_Arbeitsordner_Azubis\Fachinformatiker\Ausbildungsjahr 2021\PC Inventur neu Jabbusch\cncraum.csv";
			String line = "";
			ArrayList<String> nameOfPCs = new ArrayList<String>();
			
			try {
				BufferedReader br = new BufferedReader(new FileReader(path));
				
				while((line = br.readLine()) != null) {
					String[] values = line.split(";");
					nameOfPCs.add(values[0]);
					count++;
					PCliste.put(nameOfPCs.get(count), new PC(values[0], values[1], values[2], values[3], values[4], values[5]));
				}
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String s = "";
			
			if(event.getSource() == button) {
				for(int i = 0; i < nameOfPCs.size(); i++) {
					s += "<html><br/> <html>" + PCliste.get(nameOfPCs.get(i)).getInventarnummer();
				}
				JOptionPane.showMessageDialog(null, s);
			}
		}
		
	}
	
	public static void main(String[] args) throws IOException {
				
		new TestGUI();
		
	}	
}