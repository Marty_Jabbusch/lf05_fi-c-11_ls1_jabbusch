package Work.motorradrennenGame;

public class Manager extends Person{

	private double monatlichesGehalt;
	
	public Manager (String name, String geburtsdatum, double monatlichesGehalt) {
		this.setName(name);
		this.setGeburtsdatum(geburtsdatum);
		this.monatlichesGehalt = monatlichesGehalt;
	}
	
	public double getMonatlichesGehalt() {
		return monatlichesGehalt;
	}
	
	public void setMonatlichesGehalt(double monatlichesGehalt) {
		this.monatlichesGehalt = monatlichesGehalt;
	}
	
	
	
}
