package NonRepeats;
import java.util.ArrayList;
import java.lang.Math.*;


public class NonRepeatsOptimized {

    public static int nonRepeats(int base){
        
        ArrayList<Integer> baseNumbers = new ArrayList<Integer>();
        int count = 0;
        int count2 = 0;
        int count3 = 0;
        int temp = 0;
        int modulo = 0;
        int multiplier = 0;
        int minus = 0;

        for(int i = 1; i < (int) Math.pow(base, base); i++){

            modulo = 0;
            multiplier = 0;
            minus = 0;
            count = 0;
            count2 = 0;
            temp = i;
            baseNumbers.clear();

            for(int k = (int) (Math.log(i)/Math.log(base)); k >= 0; k--){
                
                count = 0;
                
                modulo = temp % (int) Math.pow(base, k);
                multiplier = (temp - modulo) / ((int) Math.pow(base, k));
                minus = temp - modulo;
                temp = temp - minus;
                baseNumbers.add(multiplier);
                
                for(int n = 0; n < baseNumbers.size(); n++){
                    if(multiplier == baseNumbers.get(n)){
                        count++;
                    }
                    if(count > 1){
                        break;
                    }
                }
                
                if(count > 1){
                    count2++;
                    break;
                }              
            }
            if(count2 == 0){
                count3++;
            }
        }
        return count3;
    }

    public static void main(String[] args){
        System.out.println(nonRepeats(2));
    }
}
