package AndereProgramme;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files

public class ReadFile {
	
public static String readFile() throws FileNotFoundException {
	String sentence = "";
	  File myObj = new File("woerterliste.txt");
    Scanner myReader = new Scanner(myObj);
    while (myReader.hasNextLine()) {
      sentence += myReader.nextLine();
    }
    myReader.close();
    return sentence;
}
  public static void main(String[] args) throws FileNotFoundException {
      String sentence = ReadFile.readFile();
      System.out.println(sentence);
}

  
}