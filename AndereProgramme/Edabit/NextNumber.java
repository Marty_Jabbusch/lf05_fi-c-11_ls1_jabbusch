import java.lang.Math;

public class NextNumber {

    public static int[] Swap(int[] Array, int x, int y) {
        int i = Array[x];
        Array[x] = Array[y];
        Array[y] = i;
        return Array;
    }

    public static int[] SortAscending(int[] Array) {
        boolean b = false;
        do {
            b = false;
            for (int i = 1; i < Array.length; i++) {
                if (Array[i - 1] > Array[i]) {
                    Array = Swap(Array, i - 1, i);
                    b = true;
                }
            }
        } while (b == true);
        return Array;
    }

    public static int NextLargestNumber(int number) {
        boolean b = false;
        int initialNumber = number;
        int nextNumber = 0;
        int count = (int) Math.log10(number);
        int[] digits = new int[(int) (Math.log10(number) + 1)];
        int[] digitsSortedAscending = new int[(int) (Math.log10(number) + 1)];
        while (number > 0) {
            digits[count] = (number % 10);
            number = number / 10;
            count--;
        }
        digitsSortedAscending = SortAscending(digits);
        do{
            for(int i = 0; i < digits.length; i++){
                for(int n = 0; n < digits.length; n ++){
                    //immer gucken ob es hinter der ziffer höhere ziffern gibt wenn ja die niedrigst tauschen
                }
            }
        }
        while(nextNumber < number);
        return nextNumber;
    }

    public static void main(String[] args) {
        System.out.println(NextLargestNumber(2432349));
    }
}
/*
do {
            b = false;
            for (int i = 0, k = (int) Math.pow(10, digits.length - 1); i < digits.length && k >= 1; i++, k = k / 10) {
                nextNumber += k * digits[i];
            }
            if (nextNumber > initialNumber) {
                b = true;
            } 
            else {
                count2++;
                digits = Swap(digits, count2 - 1, count2);
                nextNumber = initialNumber;
            }
        } while (false);
*/