package Schulaufgaben.Schleifen;

import java.util.Scanner;

class FahrkartenautomatKomplett {
	/*
	 * . 1. und 2.) double zuZahlenderBetrag; Subtraktion, Mulitplikation double
	 * eingezahlterGesamtbetrag; Subtrkation double eingeworfeneM�nze; keine double
	 * r�ckgabebetrag; keine int anzahlFahrkarten; Multiplikation 3.) siehe Code 4.)
	 * teste Code 5.) Da es nur ganze Tickets gibt, kann es sich nur um einen
	 * Integer bei anzahlFahrkarten handeln. 6.) Zeile 34: zuZahlenderBetrag =
	 * zuZahlenderBetrag * anzahlFahrkarten; der zu zahlende Betrag wird neu
	 * initialisiert und zwar mit dem Produkt aus dem Preis einer Karte und der
	 * Anzahl der Karten, das bedeutet, dass der neue Betrag der Preis von allen
	 * Karten zusammen ist.
	 *  
	 */
	public static double fahrkartenauswahl(Scanner tastatur) {
			boolean b = true;
			double preis = 0;
			double preisGesamt = 0;
			while(b == true) {
				System.out.println("Welchen Fahrschein m�chten sie kaufen?");
				System.out.println("1: Fahrschein A: 1,40�");
				System.out.println("2: Fahrschein B: 1,00�");
				System.out.println("3: Fahrschein C: 1,20�");
				System.out.println("4: Fahrschein AB: 2,60�");
				System.out.println("4: Fahrschein BC: 2,60�");
				System.out.println("5: Fahrschein ABC: 3,00�");
				String fahrscheinnummer = tastatur.next();
				while(!fahrscheinnummer.equals("A") && !fahrscheinnummer.equals("B") && !fahrscheinnummer.equals("C") && !fahrscheinnummer.equals("AB") && !fahrscheinnummer.equals("BC") && !fahrscheinnummer.equals("ABC")) {
					System.out.println("Error: Diesen Fahrschein gibt es nicht");
					fahrscheinnummer = tastatur.next();
				}
				if(fahrscheinnummer.equals("A")) {
					preis = 1.4;
				}
				if(fahrscheinnummer.equals("B")) {
					preis = 1;
				}
				if(fahrscheinnummer.equals("C")) {
					preis = 1.2;
				}
				if(fahrscheinnummer.equals("AB")) {
					preis = 2.6;
				}
				if(fahrscheinnummer.equals("BC")) {
					preis = 2.6;
				}
				if(fahrscheinnummer.equals("ABC")) {
					preis = 3;
				}
				System.out.println("Wie viele Karten m�chten Sie kaufen:");
				int anzahl = 0;
				do {anzahl = tastatur.nextInt();
					if(anzahl > 10) {
					System.out.println("Error: Bitte bestellen sie maximal 10 Tickets");
					}
				}
				while(anzahl > 10);
				
				preis = preis * anzahl;
				preisGesamt += preis;
				System.out.printf("Preis: %.2f �", preisGesamt);
				System.out.println("M�chten Sie weitere Karten kaufen: \"Ja\" \"Nein \"");
				String antwort = tastatur.next();
				if(antwort.equals("Ja")) {
					b = true;
				}
				if(antwort.equals("Nein")) {
					b = false;
				} 
			}	
			return preisGesamt;
	
		}

	/*
	 * public static double fahrkartenbestellungErfassen(Scanner tastatur) { double
	 * zuZahlenderBetrag; int anzahlFahrkarten;
	 * System.out.print("Zu zahlender Betrag (EURO): "); zuZahlenderBetrag =
	 * tastatur.nextDouble();
	 * System.out.println("Wie viele Fahrkarten m�chten sie bestellen?");
	 * anzahlFahrkarten = tastatur.nextInt();
	 * System.out.println("Anzahl der Tickets: " + anzahlFahrkarten);
	 * zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten; return
	 * zuZahlenderBetrag; }
	 */
	public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0;
		double eingeworfeneMuenze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			if (eingeworfeneMuenze > 2 || eingeworfeneMuenze < 0) {
				System.out.println("Es gibt kein " + eingeworfeneMuenze + "�m�nze");
			} else {
				eingezahlterGesamtbetrag += eingeworfeneMuenze;
			}
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrscheinausgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldberechnungUndAusgabe(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rueckgabebetrag;
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.00;
			}
			while (rueckgabebetrag >= 1.00) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.00;
			}
			while (rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.50;
			}
			while (rueckgabebetrag >= 0.20) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.20;
			}
			while (rueckgabebetrag >= 0.10) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.10;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			} 
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		boolean b = true;
		// double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

		while (b == true) {
			double fahrkartenpreis = fahrkartenauswahl(tastatur);

			double eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, fahrkartenpreis);

			fahrscheinausgabe();

			rueckgeldberechnungUndAusgabe(fahrkartenpreis, eingezahlterGesamtbetrag);
		} 

	}
}
