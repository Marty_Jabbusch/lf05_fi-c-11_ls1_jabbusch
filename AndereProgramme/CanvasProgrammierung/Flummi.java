import java.awt.*;
import javax.swing.*;

public class Flummi extends Canvas{
    public void paint(Graphics g) {
        gsetColor(Color.BLACK);
        g.drawLine(20, 20, 400, 400);
        g.setColor(Color.GREEN);
        g.drawRect(20, 4, 200, 300);
        g.drawOval(5, 100, 50, 500);
    }

    public static void main(String[] args){
        JFrame jf = new JFrame("Canvas");
        Flummi f = new Flummi();
        jf.setSize(600, 400);
        jf.setVisible(true);
        jf.setDefaultCloseOperation((JFrame.EXIT_ON_CLOSE));
        jf.add(f);
    }
}
