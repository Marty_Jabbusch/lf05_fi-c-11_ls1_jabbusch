package Work.motorradrennenGame;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;	
import java.util.HashMap;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;


public class Konsolenapplikation {
	
	HashMap<Integer, String> FahrerMap = new HashMap<Integer, String>();
	HashMap<String, String> Teams = new HashMap<String, String>();
	static HashMap<String, Motorrad> Motorradliste = new HashMap<String, Motorrad>();
	static HashMap<String, Manager> Managerliste = new HashMap<String, Manager>();
	public static HashMap<String, Team> Teamliste = new HashMap<String, Team>();
	static HashMap<String, Fahrer> Fahrerliste = new HashMap<String, Fahrer>();


	public static String managerErstellen() throws InterruptedException {
		System.out.println("Wie soll der Manager hei�en?");
		Scanner scanner1 = new Scanner(System.in);
		String eingabe11 = scanner1.next();
		System.out.println("Wann hat " + eingabe11 + " Geburtstag? \"dd.mm.yyyy\"" );
		Scanner scanner2 = new Scanner(System.in);
		String eingabe3 = scanner2.next();
		System.out.println("Wie viel Geld soll der Manager monatilich verdienen?");
		Scanner scanner3 = new Scanner(System.in);
		double eingabe4 = scanner3.nextDouble();
		Managerliste.put(eingabe11, new Manager(eingabe11, eingabe3, eingabe4));
		System.out.println("Der Manager wurde erstellt");
		
		TimeUnit.SECONDS.sleep(2);
		return eingabe11;
	}
	
	public static void teamErstellen() throws InterruptedException {
		System.out.println("Wie soll das Team hei�en?");
		Scanner scanner1 = new Scanner(System.in);
		String eingabe2 = scanner1.next();
		System.out.println("Wie soll das K�rzel des Teams " + eingabe2 + " sein?");
		Scanner scanner2 = new Scanner(System.in);
		String eingabe3 = scanner2.next();
		System.out.println("Wo kommt das Team her?");
		Scanner scanner3 = new Scanner(System.in);
		String eingabe4 = scanner3.next();
		System.out.println("Das Team muss von einem Manager gemanaged werden, m�chten Sie einen erstellen oder einen bereits vorhandenen w�hlen? \"Neu\" oder4 \"Alt\"");
		Scanner scanner4 = new Scanner(System.in);
		String eingabe5 = scanner4.next();
		if(eingabe5.equals("Alt")) {
			System.out.println("Welcher Manager soll das Team managen?");
			boolean b = false;
			while (b == false) {
				Scanner scanner5 = new Scanner(System.in);
				String eingabe6 = scanner5.next();
				if(Managerliste.get(eingabe6) == null) {
					System.out.println("Diesen Manager gibt es nicht!");
				}
				else{
				Teamliste.put(eingabe2, new Team(Managerliste.get(eingabe6), eingabe2, eingabe3, eingabe4));
				b = true;
				}
			}
		}
		if(eingabe5.equals("Neu")) {
			String a = managerErstellen();
			Teamliste.put(eingabe2, new Team(Managerliste.get(a), eingabe2, eingabe3, eingabe4));
		}
		System.out.println("Das Team " + eingabe2 + " wurde erstellt");
		TimeUnit.SECONDS.sleep(2);

	}
	
	public static void motorradErstellen() throws InterruptedException {
		System.out.println("Von welchem Hersteller ist das Motorrad?");
		Scanner scanner1 = new Scanner(System.in);
		String eingabe2 = scanner1.next();
		System.out.println("Was f�r ein Modell ist das Motorrad?");
		Scanner scanner2 = new Scanner(System.in);
		String eingabe3 = scanner2.next();
		String Motorradname = eingabe2 + eingabe3;
		Motorradliste.put(Motorradname, new Motorrad());
		Motorradliste.get(Motorradname).setModell(eingabe3);
		Motorradliste.get(Motorradname).setHersteller(eingabe2);
		System.out.println("Wann ist das Herstellungsdatum?");
		Scanner scanner3 = new Scanner(System.in);
		String eingabe4 = scanner3.next();
		Motorradliste.get(Motorradname).setHerstellungsdatum(eingabe4);
		System.out.println("Wann wurde es gekauft? (Datum)");
		Scanner scanner4 = new Scanner(System.in);
		String eingabe5 = scanner4.next();
		Motorradliste.get(Motorradname).setEinkaufsdatum(eingabe4);
		System.out.println("Wie viel PS hat das Motorrad?");
		Scanner scanner5 = new Scanner(System.in);
		int eingabe6 = scanner5.nextInt();
		Motorradliste.get(Motorradname).setPs(eingabe6);
		System.out.println("Was ist die maximale Geschwindigkeit?");
		Scanner scanner6 = new Scanner(System.in);
		int eingabe7 = scanner6.nextInt();
		Motorradliste.get(Motorradname).setMaxSpeed(eingabe7);
		System.out.println("Wie gro� ist der Hubraum?");
		Scanner scanner7 = new Scanner(System.in);
		int eingabe8 = scanner7.nextInt();
		Motorradliste.get(Motorradname).setHubraum(eingabe8);
		System.out.println("Wie ist der Kilometerstand?");
		Scanner scanner8 = new Scanner(System.in);
		int eingabe9 = scanner8.nextInt();
		Motorradliste.get(Motorradname).setHubraum(eingabe8);
		System.out.println("Das Motorrad wurde erstellt");
		TimeUnit.SECONDS.sleep(2);
		}
	
	public static void fahrerErstellen() throws InterruptedException {
		System.out.println("Wie soll der Fahrer hei�en?");
		Scanner scanner1 = new Scanner(System.in);
		String eingabe2 = scanner1.next();
		System.out.println("Wann hat " + eingabe2 + " Geburtstag? \"dd.mm.yyyy\"" );
		Scanner scanner2 = new Scanner(System.in);
		String eingabe3 = scanner2.next();
	    Fahrerliste.put(eingabe2, new Fahrer(eingabe2, eingabe3));
	    System.out.println("Soll der Fahrer einem Team beitreten? \"Ja\" oder \"Nein\"");
	    Scanner scanner3 = new Scanner(System.in);
		String eingabe4 = scanner3.next();
		if(eingabe4.equals("Nein")) {
			System.out.println("Der Fahrer ist keinem Team beigetreten");
		}
		if(eingabe4.equals("Ja")) {
			System.out.println("Soll er einem bestehendem Team beitreten oder soll ein neues Team erzeugt werden? \"Neu\" oder \"Alt\"");
			Scanner scanner9 = new Scanner(System.in);
			String eingabe10 = scanner9.next();
			if(eingabe10.equals("Alt"))
			{
				System.out.println("Welchem Team soll der Fahrer beitreten? \"Teamname\"");
				Scanner scanner8 = new Scanner(System.in);
				String eingabe9 = scanner8.next();
				Teamliste.get(eingabe9).setFahrerliste(eingabe2, Fahrerliste.get(eingabe2));
			}
			if(eingabe10.equals("Neu")) {
				teamErstellen();
			}
		}
		System.out.println("Soll der Fahrer ein Motorrad fahren? \"Ja\" oder \"Nein\"");
		Scanner scanner4 = new Scanner(System.in);
		String eingabe5 = scanner4.next();
		if(eingabe5.equals("Ja")) {
			System.out.println("Soll er ein neues Motorrad fahren oder ein bereits erstelltes? \"Neu\" oder \"Alt\"");
			Scanner scanner5 = new Scanner(System.in);
			String eingabe6 = scanner5.next();
			if(eingabe6.equals("Neu")){
				motorradErstellen();
			}
			
			if(eingabe6.equals("Alt")) {
				System.out.println("Welche Marke?");
				Scanner scanner6 = new Scanner(System.in);
				String eingabe7 = scanner6.next();
				System.out.println("Welches Modell?");
				Scanner scanner7 = new Scanner(System.in);
				String eingabe8 = scanner7.next();
				String motorradname = eingabe7 + eingabe8;
				Fahrerliste.get(eingabe2).setMotorradliste(motorradname, Motorradliste.get(motorradname));
				
			}
		}
		System.out.println("Der Fahrer " + eingabe2 + " wurde erstellt.");
	}

	public static int[] motorradWerteVonFahrerAusTeam (String eingabe3) {
		System.out.println("Welcher Fahrer des Teams " + eingabe3 + " soll antreten? Gib die Nummer ein");
		Fahrerliste.entrySet().forEach(entry -> {
		    System.out.println(entry.getKey() + " " + entry.getValue().getName()); //+ " f�hrt " + Fahrerliste.get(entry.getValue().getName()).getMotorradliste(a));
		});
		Scanner scanner4 = new Scanner(System.in);
		int eingabe5 = scanner4.nextInt();
		List<String> motorradArrayList = new ArrayList<String>(Motorradliste.keySet());
		System.out.println("Welches Motorrad soll er nutzen? Gib den \"Namen\" ein");
		int sum2 = Motorradliste.size();
		for(int i = 0; i < sum2; i++) {
			System.out.println("Name: " + motorradArrayList.get(i));
		}
		Scanner scanner5 = new Scanner(System.in);
		String eingabe6 = scanner5.next();
		System.out.print("Das Motorrad " + eingabe6 + "von " + eingabe5 + " aus dem Team " + eingabe3 + "wurde ausgew�hlt. ");
		int hubraum = Teamliste.get(eingabe3).getFahrerliste(eingabe5).getMotorradliste(eingabe6).getHubraum();
		int maxSpeed = Teamliste.get(eingabe3).getFahrerliste(eingabe5).getMotorradliste(eingabe6).getMaxSpeed();
		int ps = Teamliste.get(eingabe3).getFahrerliste(eingabe5).getMotorradliste(eingabe6).getPs();
		int[] motorradWerte = {hubraum, maxSpeed, ps};
		return motorradWerte;
	}
	

	public static void main(String[] args) throws ParseException, InterruptedException {

		
		boolean b = true;
		
		while(b == true) {
			System.out.println("Dr�cke 0 wenn: Ein Rennen starten");
			System.out.println("Dr�cke 1 wenn: Manager erstellen");
			System.out.println("Dr�cke 2 wenn: Motorrad erstellen");
			System.out.println("Dr�cke 3 wenn: Team erstellen");
			System.out.println("Dr�cke 4 wenn: Fahrer erstellen");
			System.out.println("Dr�cke 5 wenn: erstellte Objekte anzeigen");

			

			Scanner scanner = new Scanner(System.in);
			int eingabe1 = scanner.nextInt();
			if(eingabe1 == 0) {
				b = false;
			}
			
			if (eingabe1 == 1) {
				managerErstellen();
			}
			
			if (eingabe1 == 2) {
				motorradErstellen();
			}
			
			if (eingabe1 == 3) {
				teamErstellen();
			}
			
			if (eingabe1 == 4) {
				fahrerErstellen();
			}
			
			if (eingabe1 == 5) {
				Fahrerliste.entrySet().forEach(entry -> {
				    System.out.println("Fahrer: " + entry.getValue().getName()); //+ " f�hrt " + Fahrerliste.get(entry.getValue().getName()).getMotorradliste(a));
				});
				Motorradliste.entrySet().forEach(entry1 -> {
					System.out.println("Motorrad: " + entry1.getValue().getHersteller() + " " + entry1.getValue().getModell()); //+ " wird gefahren von " + entry1.getValue().getGefahrenVon(0) + entry1.getValue().getGefahrenVon(1));
				});
				Managerliste.entrySet().forEach(entry2 -> {
					System.out.println("Manager: " + entry2.getValue().getName());
				});
				
				Teamliste.entrySet().forEach(entry3 -> {
					System.out.println("Team: " + entry3.getValue().getName() +  ", gemanaged von " + entry3.getValue().getManager().getName());
				});
				TimeUnit.SECONDS.sleep(3);

			}
			
			if (eingabe1 == 0) {
				System.out.println("Es k�nnen maximal 5 Teams im Rennen gegeneinander antreten? Wie viele sollen antreten?");
				Scanner scanner1 = new Scanner(System.in);
				int eingabe2 = scanner1.nextInt();
				if (eingabe2 == 2) {
					System.out.println("Welches ist das erste Team? \"Name\"");
					Scanner scanner2 = new Scanner(System.in);
					String eingabe3 = scanner2.next();
					System.out.println("Welches ist das zweite Team? \"Name\"");
					Scanner scanner3 = new Scanner(System.in);
					String eingabe4 = scanner3.next();
					int[] motorradWerte = motorradWerteVonFahrerAusTeam(eingabe3);
					int hubraum = motorradWerte[0];
					int maxSpeed = motorradWerte[1];
					int ps = motorradWerte[2];
					System.out.println(" Es hat einen" + hubraum + "er Hubraum, " + maxSpeed + "km/h Topspeed und " + ps + " PS");

					
				}
				if (eingabe2 == 3) {
					System.out.println("Welches ist das erste Team? \"Name\"");
					Scanner scanner2 = new Scanner(System.in);
					int eingabe3 = scanner2.nextInt();
					System.out.println("Welches ist das zweite Team? \"Name\"");
					Scanner scanner3 = new Scanner(System.in);
					int eingabe4 = scanner3.nextInt();
				}
				if (eingabe2 == 4) {
					
				}
				if (eingabe2 == 5) {
					
				}
			}
		}
	}
}
	