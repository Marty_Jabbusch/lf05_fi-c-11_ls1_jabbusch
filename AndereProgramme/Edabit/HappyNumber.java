import java.util.ArrayList;

public class HappyNumber {

    public static boolean isHappy(int number) {

        boolean b = false;
        boolean b2 = false;
        int length = 0;
        int temp = 0;
        int sum = 0;
        int initialNumber = number;
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        while (b == false) {

            length = String.valueOf(number).length();
            int[] digits = new int[length];
            sum = 0;
            temp = 0;

            for (int i = 0, k = length; i < length; i++, k--) {
                number = number - temp;
                temp = number % (int) Math.pow(10, i + 1) / (int) Math.pow(10, i);
                digits[k - 1] = temp;

            }
            for (int i = 0; i < digits.length; i++) {
                digits[i] = digits[i] * digits[i];
            }
            for (int i = 0; i < digits.length; i++) {
                sum = sum + digits[i];
            }
            System.out.println(sum);

            for (int i = 1; i < numbers.size(); i++) {
                if (sum == numbers.get(i)) {
                    b = true;
                    break;
                }
            }
            if (sum == 1 || sum == initialNumber) {
                b = true;
            } 
            number = sum;
            numbers.add(sum);
        }

        if (number == 1) {
            b2 = true;
        }
        return b2;
    }

    public static void main(String[] args) {
        System.out.println(isHappy(2871));
    }

}
