public class KaprekarsConstant {

    public static int kaprekar(int number) {
        int count = 0;
        int count2 = 0;
        int kaperkarsNumber = 0;
        int length = String.valueOf(number).length();
        int temp = 0;
        
        while(kaperkarsNumber != 6174){
            
            count = 0;
            temp = 0;
            int[] digits1 = new int[length + (4 - length)];
            int[] digits2 = new int[length + (4 - length)];
            int[] numbers = new int[2];

            while (number > 0) {
                digits1[count] = number % 10;
                number = number / 10;
                count++;
            }
            for (int i = 0; i < digits1.length; i++) {
                for (int n = 0; n < digits1.length; n++) {
                    while (digits1[i] > digits1[n]) {
                        temp = digits1[i];
                        digits1[i] = digits1[n];
                        digits1[n] = temp;
                    }
                }
            }
            for (int i = digits1.length - 1, n = 0; i >= 0; i--, n++) {
                digits2[n] = digits1[i];
            }
            for (int i = 0; i < digits1.length; i++) {
                numbers[0] = numbers[0] + (digits1[i] * (int) Math.pow(10, i));
            }
            for (int i = 0; i < digits2.length; i++) {
                numbers[1] = numbers[1] + (digits2[i] * (int) Math.pow(10, i));
            }

            kaperkarsNumber = numbers[1] - numbers[0];
            number = kaperkarsNumber;
            count2++;
        }        
        
        return count2;
    }

    public static void main(String[] args) {
        System.out.println(kaprekar(1234));
    }
}
