package Schulaufgaben.Methoden;
import java.util.Scanner;

public class PCHaendler {

	public static String liesString(Scanner myScanner, String text) {
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(Scanner myScanner, String text) {
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(Scanner myScanner, String text) {
		System.out.println(text);
		double nettopreis = myScanner.nextDouble();
		return nettopreis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double
	nettopreis){
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, 
	double mwst) {
		double gesamtbruttopreis = nettogesamtpreis * (1 + mwst / 100);
		return gesamtbruttopreis;
	}
	public static void rechnungsausgaben(String artikel, int anzahl, double 
	nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	
	
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		//Eingaben
		String artikel = liesString(myScanner, "Was m�chten Sie bestellen?");
		int anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein:");
		double nettopreis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
		double mwst = liesDouble(myScanner, "Geben Sie den Mehrwretsteuersatz in Prozent ein:");
		
		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungsausgaben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}