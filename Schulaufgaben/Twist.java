package Schulaufgaben;

import java.util.Random;
import java.io.*;
import java.util.Scanner;
import AndereProgramme.RNGwithoutDuplications;
import java.util.Arrays;

public class Twist {

	public static boolean numberInArray(int[] array, int number) {
		int count = 0;
		for(int i = 0; i < array.length; i++) {
			if(array[i] == number) {
				count++;
			}
		}
		if(count == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static boolean letterInString(String s, char letter) {
		int count = 0;
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == letter) {
				count++;
			}
		}
		if(count == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static int[] rng(int zahlenmenge, int max, int min) {
		int[] randomArray = new int[zahlenmenge];
		int randomNum;
		int loop = 1;
		for(int i = 0; i < randomArray.length; i++, loop = 1) {
			do {
				Random r = new Random();
				randomNum = r.nextInt((max - min) +1) + min;
				if(numberInArray(randomArray, randomNum) == false || i == 0) {
					randomArray[i] = randomNum;
					loop = 0;
				}
			}
			while(loop == 1);
		}
		return randomArray;
	}
	
	public static char[] selectedLetters(String wort) {
		char[] buchstaben = new char[wort.length()];
		for(int i = 1; i < (wort.length()-1); i++) {
			buchstaben[i] = wort.charAt(i);
		}
		return buchstaben;
	}
	
public static String eraseDuplicateLetters(String word) {
	int count = 0;
	String newWord = "";
	String duplicateLetters = "";
	String lettersWitoutDuplication = "";
	char letterNotDuplicated;
	for(int i = 0; i < word.length(); i++) {
		count = 0;
		for(int z = 0; z < word.length(); z++) {
			if(word.charAt(i) == word.charAt(z))
				count++;
		}
		if(count > 1 && Twist.letterInString(duplicateLetters, word.charAt(i)) == false){
			duplicateLetters += word.charAt(i);
		}
	}
	if(duplicateLetters.length() >= 1) {
		for(int i = 0; i < duplicateLetters.length(); i++) {
			for(int z = 0; z < word.length(); z++) {
				if(duplicateLetters.charAt(i) != word.charAt(z) && letterInString(duplicateLetters, word.charAt(z)) == false) {
					letterNotDuplicated = word.charAt(z);
					duplicateLetters += letterNotDuplicated;
				} 
			}
		}
		newWord = lettersWitoutDuplication + duplicateLetters;		
	}
	else {
		newWord = word;
	}
	return newWord;
}
	
	public static String twistingChars(char[] letters, int[] numbers) {
		String twistedWord = "";
		char[] twistedLetters = new char[letters.length];
		for(int i = 0; i < numbers.length; i++) {
			twistedLetters[i] = letters[numbers[i]];
		}
		for(int i = 0; i < numbers.length; i++) {
			twistedWord += twistedLetters[i];
		}
		return twistedWord;
	}
	
	public static String twistWord(String eingabe) {
		String twistedWord = ""; 
		
		if(eingabe.length() == 1) {
			twistedWord = eingabe;
		}
		if(eingabe.length() == 2) {
			twistedWord = eingabe;
		}
		if(eingabe.length() == 3) {
			twistedWord = eingabe;
		}
		if(eingabe.length() > 3) {
			do {
			char[] lettersArray = Twist.selectedLetters(eingabe);
			int[] randomNumbersArray = Twist.rng((lettersArray.length - 2), (lettersArray.length - 2), 1);
			twistedWord = "";
			twistedWord += eingabe.charAt(0);
			twistedWord += Twist.twistingChars(lettersArray, randomNumbersArray);
			twistedWord += eingabe.charAt(((eingabe.length())-1));
			}
			while(eingabe.equals(twistedWord));
		}
		return twistedWord;
	}
	
	public static String twistSentence(String sentence) {
		String twistedSentence = "";
		String[] words = new String[sentence.length()];
		words = sentence.split("\\W+");
		for(int i = 0; i < words.length; i++) {
			String twistedWord = Twist.twistWord(words[i]);
			twistedSentence += twistedWord + " ";
		}
		return twistedSentence;
	}
	
	public static String enttwister(String twistedSentence) throws FileNotFoundException {
		int count = 0;
		int count2 = 0;
        String st;
		String sentence = "";
		String currentWord = "";
		String currentWordWitoutDuplicates = "";
		String twistedWordWithoutDuplicates = "";
		String[] twistedWords = new String[twistedSentence.length()];
		twistedWords = twistedSentence.split("\\W+");
		for(int i = 0; i < twistedWords.length; i++) {
			File file = new File(
<<<<<<< Updated upstream
			"C:\\FilesForProgramming\\woerterliste.txt");
=======
<<<<<<< HEAD
			"C:\\Users\\pp576\\Repository\\lf05_fi-c-11_ls1_jabbusch\\Schulaufgaben\\woerterliste.txt");
=======
			"C:\\FilesForProgramming\\woerterliste.txt");
>>>>>>> b712bc9b448bfad528906fd7bdb542c305ac44a2
>>>>>>> Stashed changes

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			while ((st = br.readLine()) != null){
				currentWord = st;
				  currentWord = currentWord.toLowerCase();
				  twistedWords[i] = twistedWords[i].toLowerCase();
				  if(currentWord.charAt(0) == twistedWords[i].charAt(0) && currentWord.charAt((currentWord.length()-1)) == twistedWords[i].charAt((twistedWords[i].length()-1)) && twistedWords[i].length() == currentWord.length()) {
					  count = 0;
					  twistedWordWithoutDuplicates = eraseDuplicateLetters(twistedWords[i]);
			    	  currentWordWitoutDuplicates = eraseDuplicateLetters(currentWord);
					  for(int z = 0; z < twistedWordWithoutDuplicates.length(); z++) {
						  for(int n = 0; n < currentWordWitoutDuplicates.length(); n++) {
			    			  if(twistedWordWithoutDuplicates.charAt(z) == currentWordWitoutDuplicates.charAt(n)) {
			    				  count++;
			    			  }
						  }
					  }
					  count2 = 0;
					  for(int z = 0; z < currentWordWitoutDuplicates.length(); z++) {
			    		  if(letterInString(twistedWordWithoutDuplicates, currentWordWitoutDuplicates.charAt(z)) == false) {
			    			  count2++;
			    		  }
					  }
					  if(count2 == 0) {
						  if(count == twistedWordWithoutDuplicates.length()) {
					    	  sentence += currentWord + " ";
					      }
					  }
				  }
			  }
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return sentence;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(System.in);
		String eingabe = scanner.nextLine();
		scanner.close();
		String twistedSentence = Twist.twistSentence(eingabe);
		System.out.println("Twisted Sentence: " + twistedSentence);
		String untwistedSentence = Twist.enttwister(twistedSentence);
		System.out.println("Untwisted Sentence: " + untwistedSentence);
	}

}
