public class Sortieralgorhytmus {

    public static int[] Swap(int[] Array, int x, int y) {
        int i = Array[x];
        Array[x] = Array[y];
        Array[y] = i;
        return Array;
    }

    public static int[] Sort(int[] Array) {
        boolean b = false;
        do {
            b = false;
            for (int i = 1; i < Array.length; i++) {
                if (Array[i - 1] < Array[i]) {
                    Array = Swap(Array, i - 1, i);
                    b = true;
                }  
            }
        } while (b == true);
        return Array;
    }

    public static void main(String[] args) {
        int[] Array = {2,4,1,7,9,3,6,2,6,0,2,5,7,2,1,2,54,78,4,0,4,7};
        Array = Sort(Array);
        for (int i = 0; i < Array.length; i++) {
            System.out.print(Array[i] + " ");
        }
    }
}