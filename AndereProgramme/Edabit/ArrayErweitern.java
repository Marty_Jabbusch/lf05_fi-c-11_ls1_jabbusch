
public class ArrayErweitern {

	public static void arrayErweitern(int[] Array, int a) {
		int length = Array.length;
		
		if(a<0 && a < -length) {
			System.out.println("Die eingegebene Zahl ist ung�ltig");
			System.exit(0);
		}
		
		int[] ArrayNew = new int[length + a];
		
		if(a < 0) {
			length = length + a;
		}
		
		for(int i = 0; i < length; i++) {
			ArrayNew[i] = Array[i];
		}
		
		Array = ArrayNew;
		System.out.println(Array.length);
	}
	
	public static void main(String[] args) {

		int[] Array = {1,2,3,4,5,6,7,8};
		Array = arrayErweitern(Array, 678);
		System.out.println(Array);		
	}
}
