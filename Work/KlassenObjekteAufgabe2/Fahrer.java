package Work.KlassenObjekteAufgabe2;

public class Fahrer {
    
    String name;
    int alter;
    char geschlecht;
    Motorrad motorrad;
    
    public Fahrer(String name, int alter, char geschlecht, Motorrad motorrad){
        this.name = name;
        this.alter = alter;
        this.geschlecht = geschlecht;
        this.motorrad = motorrad;
    }
}
