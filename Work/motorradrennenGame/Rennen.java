package Work.motorradrennenGame;

import java.util.ArrayList;

public class Rennen {
	private String name;
	private String datum;
	private int hubraum;
	private double teilnahmeGebuehr;
	private int rundenanzahl;
	private int rundenlaenge;
	private int preisgelder;
	private ArrayList<Team> teamsImRennen = new ArrayList<Team>();

	public void setTeamsImRennen(int a, Team b) {
		teamsImRennen.add(a, b);
	}
	
	public Team getGefahrenVon(int a) {
		return teamsImRennen.get(a);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public int getHubraum() {
		return hubraum;
	}
	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}
	public double getTeilnahmeGebuehr() {
		return teilnahmeGebuehr;
	}
	public void setTeilnahmeGebuehr(double teilnahmeGebuehr) {
		this.teilnahmeGebuehr = teilnahmeGebuehr;
	}
	public int getRundenanzahl() {
		return rundenanzahl;
	}
	public void setRundenanzahl(int rundenanzahl) {
		this.rundenanzahl = rundenanzahl;
	}
	public int getRundenlaenge() {
		return rundenlaenge;
	}
	public void setRundenlaenge(int rundenlaenge) {
		this.rundenlaenge = rundenlaenge;
	}
	public int getPreisgelder() {
		return preisgelder;
	}
	public void setPreisgelder(int preisgelder) {
		this.preisgelder = preisgelder;
	}
	
	
	
}
