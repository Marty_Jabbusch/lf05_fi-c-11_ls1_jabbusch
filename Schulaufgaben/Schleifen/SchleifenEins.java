package Schulaufgaben.Schleifen;

public class SchleifenEins {

	public static void AufgabeNeunTreppen(int h, int b) {
		
		for(int i = h; i >= 0; i--) {
			for(int n = 0; n < i*b; n++) {
				System.out.print(" ");
			}
			for(int n = 0; n < b; n++){
				for(int x = 0; x < h-i; x++) {
					System.out.print("*");
				}
			}
			
			System.out.println();
		} 
	}
	
	
	public static void main(String[] args) {
		AufgabeNeunTreppen(10, 3);
 	}

}
