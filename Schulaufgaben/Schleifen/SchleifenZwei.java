package Schulaufgaben.Schleifen;

public class SchleifenZwei {

	public static boolean DigitInNumberOrCrossSum(int number, int number2) {
		boolean b = false;
		int crossSum = 0; 
		int count = 0;
		int count2 = 0;
		int length = String.valueOf(number).length();
		int[] digits = new int[length];
		while (number > 0) {
			digits[count] = (int) (number % 10);
			number = number / 10;
			count++;
		}
		for(int i = 0; i < digits.length; i++) {
			crossSum += digits[i];
			if(number2 == digits[i]) {
				count2++;
			}
		}
		if(crossSum == number2) {
			count2++;
		}
		if(count2 > 0) {
			b = true;
		}
		
		
		return b;
	}

	public static void AufgabeAcht(int number) {
		for (int i = 1; i <= 100; i = i + 10) {
			for (int n = 0; n <= 9; n++)
				if ((i + n) % number == 0 || DigitInNumberOrCrossSum((i + n),number ) == true) {
					System.out.print("* ");
				} else {
					if ((i + n) != 100) {
						System.out.print((i + n) + " ");
					}
				}

			System.out.println();
		}
	}

	public static void main(String[] args) {
		AufgabeAcht(10); 
	}

}
