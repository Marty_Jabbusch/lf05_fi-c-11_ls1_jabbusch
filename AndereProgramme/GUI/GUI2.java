package AndereProgramme;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class GUI2 extends JFrame{
	JLabel derText;
	
	public GUI2() {
		setLayout(null);
		setVisible(true);
		setSize(800,800);
		setTitle("Titel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		derText = new JLabel("Fahrscheinautomat");
		derText.setBounds(280, 50, 250, 30);
		Font schriftart = new Font("Calibri", Font.PLAIN + Font.ITALIC, 30);
		derText.setFont(schriftart);
		add(derText);
	}
	
	public static void main(String[] args) {
		JFrame GUI2 = new GUI2();
	}

}
