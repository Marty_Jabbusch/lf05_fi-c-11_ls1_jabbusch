import java.lang.Math;

public class DisariumNumber {
    
    public static boolean IsDisarium(int number){
        boolean b = false;
        int count = (int) Math.log10(number);
        int disarium = 0; 
        int initialNumber = number;
        int[] digits = new int[(int) (Math.log10(number) + 1)];
        while (number > 0) {
            digits[count] = (number % 10);
            number = number / 10;
            count --;
        }
        for(int i = 0; i < digits.length; i++){
            disarium += Math.pow(digits[i], i+1);
        }
        if(disarium == initialNumber){
            b = true;
        }
        return b; 
    }

    public static void main (String[] args){
        System.out.println(IsDisarium(135));
    } 

}
