package Schulaufgaben.Kontrollstrukturen;
import java.util.Scanner;

public class Kontrollstrukturen{
	
	public static void bedingung1(double zahl1, double zahl2) {
		String aussage = "";
		if(zahl1 == zahl2) {
			aussage = "gleich";
		}
		if(zahl1 < zahl2) {
			aussage = "zahl1 ist kleiner als zahl2";
		}
		if(zahl1 > zahl2) {
			aussage = "zahl1 ist gr��er als zahl2";
		}
		System.out.println(aussage);
	}
	
	public static boolean bedingung2(double zahl1, double zahl2, double zahl3) {
		boolean b = false;
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			b = true;
		}
		return b;
	}
	
	public static boolean bedingung3(double zahl1, double zahl2, double zahl3) {
		boolean b = false;
		if(zahl3 > zahl2 || zahl3 > zahl1) {
			b = true;
		}
		return b;
	}
	
	public static double bedingung4(double zahl1, double zahl2, double zahl3) {
		double groessteZahl = 0;
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			groessteZahl = zahl1;
		}
		if(zahl2 > zahl1 && zahl2 > zahl3) {
			groessteZahl = zahl2;
		}
		if(zahl3 > zahl2 && zahl3 > zahl1) {
			groessteZahl = zahl3;
		}
		return groessteZahl;
	}
	
	public static double steuersatz(Scanner scanner) {
		System.out.println("Geben Sie den Nettoetrag ein:");
		double nettobetrag = scanner.nextDouble();
		double steuersatz = 0;
		double bruttobetrag = 0;
		System.out.println("Geben Sie j f�r den erm��igten und n f�r den normalen Steuersatz ein");
		char jodern = scanner.next().charAt(0);
		if(jodern == 'j') {
			steuersatz = 1.07;
		}
		if(jodern == 'n') {
			steuersatz = 1.19;
		}
		bruttobetrag = nettobetrag * steuersatz;
		System.out.println(bruttobetrag);
		return bruttobetrag;
	}
	
	public static double hardwaregrosshaendler(double anzahl, double preis) {
		double lieferkosten = 0;
		double gesamtkosten;
		if(anzahl >= 10) {
			lieferkosten = 10;
		}
		gesamtkosten = (anzahl * preis * 1.19) + lieferkosten;
		System.out.println(gesamtkosten);
		return gesamtkosten;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double eingabe1 = scanner.nextDouble();
		double eingabe2 = scanner.nextDouble();
		hardwaregrosshaendler(eingabe1, eingabe2);
	}
}