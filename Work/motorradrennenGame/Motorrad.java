package Work.motorradrennenGame;

import java.util.ArrayList;

public class Motorrad {
	private String hersteller;
	private String modell;
	private String herstellungsdatum;
	private String einkaufsdatum;
	private int ps;
	private int maxSpeed;
	private int hubraum;
	private int kilometerstand;
	private ArrayList<Fahrer> gefahrenVon = new ArrayList<Fahrer>();
	
	
	
	public void setGefahrenVon(int a, Fahrer b) {
		gefahrenVon.add(a, b);
	}
	
	public Fahrer getGefahrenVon(int a) {
		return gefahrenVon.get(a);
	}
	
	public String getHersteller() {
		return hersteller;
	}
	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}
	public String getModell() {
		return modell;
	}
	public void setModell(String modell) {
		this.modell = modell;
	}
	public String getHerstellungsdatum() {
		return herstellungsdatum;
	}
	public void setHerstellungsdatum(String herstellungsdatum) {
		this.herstellungsdatum = herstellungsdatum;
	}
	public String getEinkaufsdatum() {
		return einkaufsdatum;
	}
	public void setEinkaufsdatum(String einkaufsdatum) {
		this.einkaufsdatum = einkaufsdatum;
	}
	public int getPs() {
		return ps;
	}
	public void setPs(int ps) {
		this.ps = ps;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getHubraum() {
		return hubraum;
	}
	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}
	public int getKilometerstand() {
		return kilometerstand;
	}
	public void setKilometerstand(int kilometerstand) {
		this.kilometerstand = kilometerstand;
	}
	
	
	
}
