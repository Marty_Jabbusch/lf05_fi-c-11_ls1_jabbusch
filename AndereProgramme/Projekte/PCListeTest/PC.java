package PCListeTest;

public class PC {

	private String geraetename;
	private String beschreibung;
	private String mietende;
	private String letzteAktualisierung;
	private String restlTage;
	
	public PC (String a, String b, String c, String d, String e, String f) {
		this.inventarnummer = a;
		this.geraetename = b;
		this.beschreibung = c;
		this.mietende = d;
		this.letzteAktualisierung = e;
		this.restlTage = f;
	}	
	
	private String inventarnummer;
	public String getInventarnummer() {
		return inventarnummer;
	}

	public void setInventarnummer(String inventarnummer) {
		this.inventarnummer = inventarnummer;
	}

	public String getGeraetename() {
		return geraetename;
	}

	public void setGeraetename(String geraetename) {
		this.geraetename = geraetename;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getMietende() {
		return mietende;
	}

	public void setMietende(String mietende) {
		this.mietende = mietende;
	}

	public String getLetzteAktualisierung() {
		return letzteAktualisierung;
	}

	public void setLetzteAktualisierung(String letzteAktualisierung) {
		this.letzteAktualisierung = letzteAktualisierung;
	}

	public String getRestlTage() {
		return restlTage;
	}

	public void setRestlTage(String restlTage) {
		this.restlTage = restlTage;
	}
	
}
	
	
