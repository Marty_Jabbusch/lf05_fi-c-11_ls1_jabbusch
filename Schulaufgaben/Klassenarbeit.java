import java.util.Scanner;

public class Klassenarbeit {

    public static int[] vertauschen(int zahl1, int zahl2){
        int temp;
        
            temp = zahl1;
            zahl1 = zahl2;
            zahl2 = temp;

            int[] zahlen = {zahl1, zahl2};
        
        return zahlen;
    }

    public static int[] ungeradeZahl(int zahl1, int zahl2){
        int[] ungeradeZahlen = new int[zahl2-zahl1];
        int i = 0;
        if(zahl1 % 2 == 0){
            while(zahl1 + 1 <= zahl2){
                ungeradeZahlen[i] = zahl1 + 1;
                zahl1 = zahl1 + 2;
                i++;
            }
        }
        else{
            while(zahl1 <= zahl2){
                ungeradeZahlen[i] = zahl1;
                zahl1 = zahl1 + 2;
                i++;
            }
        }
        return ungeradeZahlen;
    }

    public static void main(String[] args){
        boolean b = false;
        Scanner scanner = new Scanner(System.in);

        while(b == false){
            System.out.print("Geben Sie bitte die erste Zahl ein: ");
            int zahl1 = scanner.nextInt();
            System.out.print("Geben Sie bitte die zweite Zahl ein: ");
            int zahl2 = scanner.nextInt();
            int[] zahlen = {zahl1, zahl2};
            if(zahl1 == zahl2){
                System.out.println("Beide eingegebenen Zahlen sind gleich groß.");
            }
            else{
                b = true;
                if(zahlen[0] > zahlen[1]){
                    zahlen  = vertauschen(zahlen[0], zahlen[1]);
                }
                zahlen = ungeradeZahl(zahlen[0], zahlen[1]);
                System.out.println("Ungerade Zahlen:");
                for(int i = 0; i < zahlen.length; i++){
                    if(zahlen[i] != 0){
                        System.out.println(zahlen[i]);
                    }
                }                
            }
        }
        scanner.close();
    }
}

