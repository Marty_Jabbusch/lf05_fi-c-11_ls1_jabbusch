public class ABArraysMitFunktionen_1 {
    
    public static String convertArrayToString(int[] numbers){
        String numbersInString = "";
        for(int i = 0; i < numbers.length; i++){
            if(i == numbers.length - 1){
                numbersInString = numbersInString + numbers[i];
            }
            else{
                numbersInString = numbersInString + numbers[i] + ",";
            }
        }
        return numbersInString;
    }

    public static void main(String[] args){
        int[] numbers = {1,2,3,4,5,6,7,8,9};
        String numbersInString = "";
        numbersInString = convertArrayToString(numbers);
        System.out.println(numbersInString);
    }


}
