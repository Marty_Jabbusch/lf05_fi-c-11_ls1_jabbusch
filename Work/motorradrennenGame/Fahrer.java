package Work.motorradrennenGame;

import java.util.ArrayList;
import java.util.HashMap;

public class Fahrer extends Person{
	private double praemieProRennen;
	private String[] bisherigePlatzierungen;
	private HashMap<String, Motorrad> motorradliste = new HashMap<String, Motorrad>();
	public Fahrer (String name, String geburtsdatum) {
		this.setName(name);
		this.setGeburtsdatum(geburtsdatum);
		}
	
	public void setMotorradliste(String a, Motorrad b) {
		motorradliste.put(a, b);
	}	
	
	public Motorrad getMotorradliste(String a) {
		return motorradliste.get(a);
	}
	
	public double getPraemieProRennen() {
		return praemieProRennen;
	}
	
	public void setPraemieProRennen(double praemieProRennen) {
		this.praemieProRennen = praemieProRennen;
	}
	public String[] getBisherigePlatzierungen() {
		return bisherigePlatzierungen;
	}
	public void setBisherigePlatzierungen(String[] bisherigePlatzierungen) {
		this.bisherigePlatzierungen = bisherigePlatzierungen;
	}
	
	
	
}
