import java.awt.*;
import javax.swing.*;

public class App extends Canvas{
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawLine(20, 20, 400, 400);
        g.setColor(Color.GREEN);
        g.drawRect(20, 4, 200, 300);
        g.drawOval(5, 100, 50, 500);
    }

    public static void main(String[] args){
        JFrame jf = new JFrame("Canvas");
        App f = new App();
        jf.setSize(600, 400);
        jf.setVisible(true);
        jf.setDefaultCloseOperation((JFrame.EXIT_ON_CLOSE));
        jf.add(f);
    }
}
