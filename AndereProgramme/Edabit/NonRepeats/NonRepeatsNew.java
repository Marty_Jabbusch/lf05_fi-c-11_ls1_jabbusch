package NonRepeats;

public class NonRepeatsNew {

    public static int NonRepeats(int radix) {
        int count = 1;
        int count2 = 0;
        for(int i = radix - 1; i > 0; i--){
            count = 1;
            for(int n = radix - 1; n >= i; n--){
                count = count * n;
            }
            count = count * (radix - 1);
            count2 = count2 + count;
        }
        return count2 + radix - 1;
    }

    public static void main(String[] args) {
        System.out.println(NonRepeats(15));
    }
}
