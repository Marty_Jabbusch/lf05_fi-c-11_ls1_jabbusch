package AndereProgramme;
import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class FahrkartenautomatGUI extends JFrame{
	static double preisGesamt = 0;
	double eingezahlt = 0;
	double gesamtEingezahlt = 0;
	double preisNeu = 0;
	double rueckgeld = 0;
	String ausgabe = "";
	String name = "";
	String soos = "";
	int anzahl10Euro = 0;
	int anzahl5Euro = 0;
	int anzahl2Euro = 0;
	int anzahl1Euro = 0;
	int anzahl50Cent = 0;
	int anzahl20Cent = 0;
	int anzahl10Cent= 0;
	int anzahl5Cent= 0;
	int anzahl2Cent= 0;
	int anzahl1Cent= 0;
	JFrame Fenster = new JFrame("Fahrkartenautomat");
	JPanel Panel = new JPanel();	
	JLabel label = new JLabel(String.format("Zu zahlen: %.2f�", preisGesamt));
	JLabel label2 = new JLabel("");	

	JButton A= new JButton("Fahrschein A");
	JButton B= new JButton("Fahrschein B");
	JButton C= new JButton("Fahrschein C");
	JButton AB= new JButton("Fahrschein AB");
	JButton BC= new JButton("Fahrschein BC");
	JButton ABC= new JButton("Fahrschein ABC");	
	JButton Bezahlen = new JButton("Bezahlen");	

	
	public FahrkartenautomatGUI() {
				
		Panel.setLayout(null);
		setLayout(null);
		Fenster.setVisible(true);
		Fenster.setSize(500, 500);
		Fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Panel.add(A);
		Panel.add(B);
		Panel.add(C);
		Panel.add(AB);
		Panel.add(BC);
		Panel.add(ABC);
		Fenster.add(Panel);
		Panel.add(label);
		Panel.add(label2);
		Panel.add(Bezahlen);
		Bezahlen.setBounds(150, 180, 140, 50);
		label.setBounds(30, 180, 140, 50);
		label2.setBounds(30, 230, 140, 185);
		A.setBounds(0, 0, 140, 50);
		B.setBounds(150, 0, 140, 50);
		C.setBounds(0, 60, 140, 50);
		AB.setBounds(150, 60, 140, 50);
		BC.setBounds(0, 120, 140, 50);
		ABC.setBounds(150, 120, 140, 50);
		
		DerHandler handler = new DerHandler();
		A.addActionListener(handler);
		B.addActionListener(handler);
		C.addActionListener(handler);
		AB.addActionListener(handler);
		BC.addActionListener(handler);
		ABC.addActionListener(handler);
		Bezahlen.addActionListener(handler);
	}
	private class DerHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			if(event.getSource()==A) 
				preisGesamt = preisGesamt + 1.4;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==B) 
				preisGesamt = preisGesamt + 1.0;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==C) 
				preisGesamt = preisGesamt + 1.2;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==AB) 
				preisGesamt = preisGesamt + 2.6;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==BC) 
				preisGesamt = preisGesamt + 2.6;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==ABC) 
				preisGesamt = preisGesamt + 3.0;
				label.setText(String.format("Zu zahlen: %.2f�", preisGesamt));
			if(event.getSource()==Bezahlen) {
				preisNeu = preisGesamt;
				while(preisGesamt > gesamtEingezahlt) {
				name = JOptionPane.showInputDialog(String.format("Zu zahlen: %.2f�", preisNeu));
				name = name.replaceAll(",",".");
				eingezahlt = Double.parseDouble(name);
				gesamtEingezahlt = gesamtEingezahlt + eingezahlt;
				System.out.println(gesamtEingezahlt);
				preisNeu = preisNeu - eingezahlt;
				label.setText(String.format("Zu zahlen: %.2f�", preisNeu));
				}
				rueckgeld = preisNeu * (-1);
				label.setText(String.format("R�ckgeld: %.2f�", rueckgeld));
				while(rueckgeld >= 10){
					anzahl10Euro ++;
					rueckgeld = rueckgeld - 10;
				}
				while(rueckgeld >= 5){
					anzahl5Euro ++;
					rueckgeld = rueckgeld - 5;
				}
				while(rueckgeld >= 2){
					anzahl2Euro ++;
					rueckgeld = rueckgeld - 2;
				}
				while(rueckgeld >= 1){
					anzahl1Euro ++;
					rueckgeld = rueckgeld - 1; 
				}
				while(rueckgeld >= 0.5){
					anzahl50Cent ++;
					rueckgeld = rueckgeld - 0.5; 
				}
				while(rueckgeld >= 0.2){
					anzahl20Cent ++;
					rueckgeld = rueckgeld - 0.2; 
				}
				while(rueckgeld >= 0.1){
					anzahl10Cent ++;
					rueckgeld = rueckgeld - 0.1; 
				}
				while(rueckgeld >= 0.05){
					anzahl5Cent ++;
					rueckgeld = rueckgeld - 0.05; 
				}
				while(rueckgeld >= 0.02){
					anzahl2Cent ++;
					rueckgeld = rueckgeld - 0.02; 
				}
				while(rueckgeld >= 0.01){
					anzahl1Cent ++;
					rueckgeld = rueckgeld - 0.01; 
				}
				label2.setText("<html>R�ckgabe:  <br/> 10-Euro: <html>" + anzahl10Euro + "<html>  <br/> 5-Euro: <html>" + anzahl5Euro + " <html>  <br/> 2-Euro: <html>" + anzahl2Euro + " <html> <br/> 1-Euro: <html>" + anzahl1Euro + " <html> <br/> 50-Cent: <html>" + anzahl50Cent + " <html> <br/> 20-Cent: <html>" + anzahl20Cent + " <html> <br/> 10-Cent: <html>" + anzahl10Cent + " <html> <br/> 5-Cent: <html>" + anzahl5Cent + " <html> <br/> 2-Cent: <html>" + anzahl2Cent + " <html> <br/> 1-Cent <html>" + anzahl1Cent);
				
			}	
		}
	}

	public static void main(String[] args) {
		new FahrkartenautomatGUI();
	}

}
