package NonRepeats;

public class NonRepeatsOptimizedSolution {

    public static int nonRepeats(int radix){

        double sum = 2;
		for (int i = 2; i < radix; i++){
			sum += 1.0/factorial(i);
		}
		return (int)((radix-1)*factorial(radix-1)*sum);
  }
	
	public static int factorial(int n){
		return n < 2 ? 1 : n*factorial(n-1);
	}
    
    public static void main(String[] args){
        System.out.println(nonRepeats(30));
    }
}
