package Schulaufgaben.Methoden;
import java.util.Scanner;

public class DreiPunktZwei {

	public static double reihenschaltung(double r1, double r2) {
		double ersatzwiderstand = r1 + r2;
		return ersatzwiderstand;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double ersatzwiderstand = (r1*r2)/(r1+r2);
		return ersatzwiderstand;
	}
	
	public static double quadrat(double x) {
		double quadrat = x*x;
		return quadrat;
	}
	
	public static double hypothenuse(double k1, double k2){
		double h = Math.sqrt(quadrat(k1) + quadrat(k2));
		return h;
	}
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Wie gro� ist die erste Kathete? (cm)");
		double k1 = myScanner.nextDouble();
		System.out.println("Wie gro� ist die erste Kathete? (cm)");
		double k2 = myScanner.nextDouble();
		double h = DreiPunktZwei.hypothenuse(k1, k2);
		System.out.println("Die Hypothenuse ist " + String.format("%.2f",h) + "cm lang");
	}

}
