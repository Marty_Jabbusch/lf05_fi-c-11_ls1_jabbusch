package Work.KlassenObjekteAufgabe2;

public class Motorrad {

    String bezeichnung;
    String spitzname;
    int ps;
    int hubraum;

    public Motorrad(String bezeichnung, String spitzname, int ps, int hubraum){
        this.bezeichnung = bezeichnung;
        this.spitzname = spitzname;
        this.ps = ps;
        this.hubraum = hubraum;
    }
} 
