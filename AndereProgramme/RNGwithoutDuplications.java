package AndereProgramme;
import java.util.Random;

public class RNGwithoutDuplications {
	
	public static void main(String[] args) {
		int[] Array = RNGwithoutDuplications.rng(10, 10, 1);
		for(int i = 0; i < 10; i++) {
			System.out.println(Array[i]);
		}
	}
	
	public static boolean numberInArray(int[] array, int number) {
		int count = 0;
		for(int i = 0; i < array.length; i++) {
			if(array[i] == number) {
				count++;
			}
		}
		if(count == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static int[] rng(int zahlenmenge, int max, int min) {
		int[] randomArray = new int[zahlenmenge];
		int randomNum;
		int loop = 1;
		for(int i = 0; i < randomArray.length; i++, loop = 1) {
			do {
				Random r = new Random();
				randomNum = r.nextInt((max - min) +1) + min;
				if(RNGwithoutDuplications.numberInArray(randomArray, randomNum) == false || i == 0) {
					randomArray[i] = randomNum;
					loop = 0;
				}
			}
			while(loop == 1);
		}
		return randomArray;
	}
	
}