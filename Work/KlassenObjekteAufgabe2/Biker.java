package Work.KlassenObjekteAufgabe2;

import Fahrer;
import Motorrad;
import java.util.Scanner;
import java.util.HashMap;

public class Biker {   

    public static void main(String[] args){

        HashMap<String, Motorrad> motorräder = new HashMap<String, Motorrad>();
        boolean b = true;

        while(b == true){
            System.out.println("Drücke: \n1 für Motorrad erstellen \n2 für Fahrer erstellen \n3 für Eingabe beenden");
            Scanner eingabe = new Scanner(System.in);
            int eingabe1 = eingabe.nextInt();
    
            if(eingabe1 == 1){
                System.out.println("Name des Motorrads:");
                String eingabe11 = eingabe.next();
                System.out.println("Spitzname:");
                String eingabe12 = eingabe.next();
                System.out.println("PS:");
                int eingabe13 = eingabe.nextInt();
                System.out.println("Hubraum:");
                int eingabe14 = eingabe.nextInt();
                Motorrad motorrad = new Motorrad(eingabe11, eingabe12, eingabe13, eingabe14);
                motorräder.put(eingabe11, motorrad);
            }
            if(eingabe1 == 2){
                System.out.println("Name des Fahrers:");
                String eingabe21 = eingabe.next();
                System.out.println("Alter:");
                int eingabe22 = eingabe.nextInt();
                System.out.println("Geschlecht \"(m/w/d)\":?");
                char eingabe23 = eingabe.next().charAt(0);
                System.out.println("Motorrad: \n(Muss schon erstellt sein \nBei falscher Eingabe wird kein Motorrad gespeichert)");
                String eingabe24 = eingabe.next();
                Fahrer fahrer = new Fahrer(eingabe21, eingabe22, eingabe23, motorräder.get(eingabe24));
            }
            if(eingabe1 == 3){
                b = false;
            }
        }
    }
}


