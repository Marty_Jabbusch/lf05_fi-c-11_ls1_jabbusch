package NonRepeats;
import java.util.ArrayList;
public class NonRepeats {

    public static int nonRepeats(int base){
           
        int count = 0;
        int count2 = 0;
        int count3 = 0;

        for(int i = 1; i < (int) Math.pow(base, base); i++){

            count2 = 0;

            ArrayList<Integer> baseNumbers = new ArrayList<Integer>();
            int temp = i;
            int exponent = (int) ((Math.log(i)/Math.log(base)));
            for(int k = exponent; k >= 0; k--){ 
                int modulo = temp % (int) Math.pow(base, k);
                int multiplier = (temp - modulo) / ((int) Math.pow(base, k));
                int minus = temp - modulo;
                temp = temp - minus;
                baseNumbers.add(multiplier);                
            }

            baseNumbers.add(-111111);
            for(int q = 0; q < baseNumbers.size(); q++){
                System.out.println(baseNumbers.get(q));
            }

            for(int n = 0; n < base; n++){
                count = 0;
                for(int k = 0; k < baseNumbers.size(); k++){
                    if(n == baseNumbers.get(k)){
                        count++;
                    }
                }
                if(count <= 1){
                    count2++;
                }
            }
            if(count2 >= base){
                count3++;
            } 
        }
        
        return count3;
    }

    public static void main(String[] args){
        System.out.println(nonRepeats(5));
    }
}
