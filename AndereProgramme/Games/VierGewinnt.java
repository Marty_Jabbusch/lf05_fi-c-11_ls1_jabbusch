import java.util.Scanner;

public class VierGewinnt {
	
	public static String[][] Grid(){
		String[][] grid = {
			{"  A ", "  B ", "  C ", "  D ", "  E ", "  F ", "  G ", "  H "},
			{" ___", " ___", " ___", " ___", " ___", " ___", " ___", " ___"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
			{"|___", "|___", "|___", "|___", "|___", "|___", "|___", "|___", "|"}, 
		};
		return grid;
	}

	public static void Printout(String[][] grid){
		for(int i = 0; i < grid.length; i++) {			
			for(int n = 0; n < grid[i].length; n++) {
				System.out.print(grid[i][n]);
				}
			System.out.println();
		}
	}

	public static String[][] GridChange(int[][] PlayerChanges,int[][] EnemyChanges, int numberOfChanges, String[][] grid){
		
		for(int i = 0; i < numberOfChanges; i++){
			grid[PlayerChanges[i][0]][PlayerChanges[i][1]] = "|_X_";
		}
		if(numberOfChanges != 0){
			for(int i = 0; i < numberOfChanges; i++){
				grid[EnemyChanges[i][0]][EnemyChanges[i][1]] = "|_O_";
			}
		}
		
		Printout(grid);

		return grid;
	}

	public static int[][] ScannerWitCellCaculator(int[][] PlayerChanges, int numberOfChanges, String[][] grid){
		Scanner scanner = new Scanner(System.in);
		int line = 9;
		int row = 0;
		boolean lowestCell = false;
		boolean b = false;
		do{
			String s = scanner.next();
			s = s.toLowerCase();
			if("a".equals(s)){
				row = 0;
				b = true;
			}
			if("b".equals(s)){
				row = 1;
				b = true;
			}
			if("c".equals(s)){
				row = 2;
				b = true;
			}
			if("d".equals(s)){
				row = 3;
				b = true;
			}
			if("e".equals(s)){
				row = 4;
				b = true;
			}
			if("f".equals(s)){
				row = 5;
				b = true;
			}
			if("g".equals(s)){
				row = 6;
				b = true;
			}
			if("h".equals(s)){
				row = 7;
				b = true;
			}
		}
		while(b == false);
		while(lowestCell == false){
			if(grid[line][row] == "|___"){
				lowestCell = true;
			}
			else{
				line--;
			}
		}
		PlayerChanges[numberOfChanges][0] = line;
		PlayerChanges[numberOfChanges][1] = row;
		return PlayerChanges;
	}

	public static int[][] Enemy(int[][] EnemyChanges, int begin, int numberOfChanges, String[][] grid){
		boolean lowestCell = false;
		int random = (int) (Math.random() * 2);
		int line = 9;
		if(numberOfChanges == 1){
			while(lowestCell == false){
				if(grid[line][begin] == "|___"){
					lowestCell = true;
				}
				else{
					line--;
				}
			}
			EnemyChanges[numberOfChanges - 1][0] = line;
			EnemyChanges[numberOfChanges - 1][1] = begin;
		}
		else{
			if(random == 1){
				
				EnemyChanges[EnemyChanges.length-1][EnemyChanges[EnemyChanges.length].length] = EnemyChanges[numberOfChanges]
				EnemyChanges[numberOfChanges][0] = EnemyChanges[numberOfChanges][0] + 1; //wrong
			}
		}
		return EnemyChanges;
	}
	
	public static void main(String[] args) {
		boolean b = false;
		int count = 0;
		int[][] PlayerChanges = new int[32][2];
		int[][] EnemyChanges = new int[32][2]; 
		String[][] grid = Grid();
		int enemyBegin = (int) (Math.random()*7);
		System.out.println(enemyBegin);
		Printout(grid);
		do{ 
			PlayerChanges = ScannerWitCellCaculator(PlayerChanges, count, grid);
			grid = GridChange(PlayerChanges, EnemyChanges, count, grid);
			count++;
			EnemyChanges = Enemy(EnemyChanges, enemyBegin, count, grid);
			grid = GridChange(PlayerChanges, EnemyChanges, count, grid);
		}
		while(b == false);
	}
}
