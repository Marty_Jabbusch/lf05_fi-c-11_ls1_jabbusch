package Work.motorradrennenGame;

import java.util.HashMap;
import java.util.ArrayList;

public class Team {
	private String name;
	private String kuerzel;
	private String ort;
	private double kontostand;
	private Manager manager;
	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	private HashMap<String, Fahrer> Fahrerliste = new HashMap<String, Fahrer>();
	private ArrayList<Rennen> rennenliste = new ArrayList<Rennen>();

	
	public Team (Manager m, String name, String kuerzel, String ort) {
		this.manager = m;
		this.name = name;
		this.kuerzel = kuerzel;	
		this.ort = ort;
	}
	
	public void setRennenliste(int a, Rennen b) {
		rennenliste.add(a, b);
	}
	
	public Rennen getRennenliste(int a) {
		return rennenliste.get(a);
	}
	
	public void setFahrerliste(String a, Fahrer b) {
		this.Fahrerliste.put(a, b);
	}
	
	public Fahrer getFahrerliste(int a) {
		return Fahrerliste.get(a);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getKuerzel(){
		return kuerzel;
	}

	public void setKuerzel(String kuerzel) {
		this.kuerzel = kuerzel;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	
	
	
}
