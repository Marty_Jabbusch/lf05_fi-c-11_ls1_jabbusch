import java.util.Scanner;

public class Annuitaetendarlehen {

	public static void main(String[] args) {
		int laufzeit = 0;
		float zinsen = 0, tilgung = 0, zinssatz = 0, betrag = 0, annuitaet = 0, restsumme = 0, gesamtsumme = 0;
		boolean abbezahlt = false;
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Darlehensbetrag in Euro.........: ");
		betrag = scanner.nextFloat();

		System.out.print("Annuität in Euro..........: ");
		annuitaet = scanner.nextFloat();

		System.out.print("Zinssatz in Prozent.............: ");
		zinssatz = scanner.nextFloat();

		System.out.println("\nJahr    Restsumme      Zinsen     Tilgung   Annuitaet");

		restsumme = betrag;
		do
		{
		  zinsen = restsumme * (zinssatz / 100);
		  if (annuitaet - zinsen > restsumme)
		  {
		    tilgung = restsumme;
		    abbezahlt = true;
		  }
		  else
		    tilgung = annuitaet - zinsen;

		  restsumme = restsumme - tilgung;
		  gesamtsumme = gesamtsumme + zinsen + tilgung;
		  laufzeit++;

		  System.out.printf("%2d %15.2f %12.2f %10.2f %10.2f \n", laufzeit, restsumme, zinsen, tilgung, (tilgung + zinsen));
		}
		while (!abbezahlt);

		System.out.printf("\nGesamtsumme: %.2f \nGesamtzinsen: %.2f\nGesamtzinssatz: %.2f", 
		  gesamtsumme, (gesamtsumme - betrag),((gesamtsumme - betrag) / gesamtsumme * 100));

	}
}

